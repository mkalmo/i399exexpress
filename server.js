'use strict';

const express = require('express');

const TaskService = require('./task-service');
const dao = new TaskService();
const app = express();

app.get('/api/tasks', getTasks);

app.listen(3000, () => console.log('Server is running on port 3000'));

function getTasks(request, response) {
    dao.getTasks()
        .then(tasks => {
            console.log(tasks);
            response.end('ok');
        });
}
