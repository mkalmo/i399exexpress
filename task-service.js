'use strict';

const Task = require('./task');

class MemTaskService {

    constructor() {
        this.tasks = [
            Task.withId('1', 'Task 1'),
            Task.withId('2', 'Task 2')
        ];
    }

    getTasks() {
        return Promise.resolve(this.clone(this.tasks));
    }

    clone(what) {
        return JSON.parse(JSON.stringify(what));
    }

    getNewId() {
        return (Math.random() + 'A').substr(2);
    }

}

module.exports = MemTaskService;
